import { sendMessage } from '../actions/messageActions';
import Store from '../configuration/store';
import './agent';

var io = require('socket.io-client/socket.io');
var socket = io("http://localhost:3126", { jsonp:false });

socket.on('append-message', (data) => {
    Store.dispatch(sendMessage(data));
});

export default socket;
