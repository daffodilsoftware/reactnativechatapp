import React, { Component } from 'react';
import {
Navigator,
StyleSheet,
View
} from 'react-native';
import Route from './route';


export default class Index extends Component{

    constructor(props){
      super(props);
    }

    componentWillMount() {
      console.disableYellowBox = true;
      console.disableRedBox = true;
    }

    /**
     *  render scene function is used to define the view is to be rendered by the navigator object.
     * */

    renderScene = (route, navigator) => {
       return (
        <View style={{flex:1}} {...route.props} {...navigator.props}> 
         <Route
           route={route}
           navigator={navigator}
           {...route.props}/>
        </View>   
       );
    }

    render(){
        return(
                <Navigator
                    initialRoute={{id : 'splashScreen'}}
                    renderScene={this.renderScene}
                    configureScene={()=> { return Navigator.SceneConfigs.FloatFromRight}} />
        );
    }
}

/**
 * styling for this component.
 * */

const styles = StyleSheet.create({
    container:{
        flex:1,
        alignItems:'center',
        justifyContent:'center'
    }
});