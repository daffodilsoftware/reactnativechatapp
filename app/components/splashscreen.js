import React, { Component } from 'react';
import {TextInput, Image, View, Text, StyleSheet } from 'react-native';



export default class SplashScreen extends Component {
    //Initializing navigator object
    constructor(props) {
      super(props);
    }
    
    //Navigation Chat Screen after SetTimeout function has collapsed
    componentWillMount() {
      setTimeout( () => {
           this.props.navigator.resetTo({id: 'chat'})
          }, 3000 
      );  

    }

    //Designing of Splash Screen
    render() {
        return(
            <View style={styles.container}>
              <Image style={styles.imgStyle} source={require('../assets/images/welcome.png')} resizeMode='contain'/>
              <Text style={styles.textStyle}>ChatsUp</Text>
            </View>
        );
    }
}


/**
 * Styling to this component.
 * */
const styles = StyleSheet.create({
  container:{
      flex:1,
      alignItems:'center',
      justifyContent:'center',
  },
  imgStyle: { 
      height:220, 
      width:340, 
  },
  textStyle: {
      fontSize:35,     
      fontStyle :'italic',
      textShadowColor:"#9E9E9E",
      fontWeight: 'bold'
  }
});

