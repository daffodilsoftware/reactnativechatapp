'use strict';
import React, {Component} from 'react';
import ChatContainer from '../containers/chatContainer/index';
import SplashScreen from '../components/splashscreen';

/**
 * This component is responsible only for navigation.
 * */
export default class Route extends Component{
    render() {
        let routeId = this.props.route.id;
        let ComponentToRender = '';

        switch (routeId) {
            case 'chat':
                ComponentToRender = ChatContainer;
                break;
            case 'splashScreen':
                ComponentToRender = SplashScreen;
                break;               
            default:
        }

        return (
            <ComponentToRender navigator={this.props.navigator} currentRoute={this.props.route} />
        );
    }
}