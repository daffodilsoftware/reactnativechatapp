import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';


/**
 * Stateless Heading component.
 * **/
const Heading = ()=>{
    return(
        <View style={styles.container}>
            <Text style={styles.textStyle}>ChatsUp</Text>
        </View>
    );
}

/**
 * Styling to this component.
 * */
const styles = StyleSheet.create({
    container:{
        height : 40,
        alignItems:'center',
        justifyContent:'center', 
        backgroundColor:'#BC0000'
    },
    textStyle:{
        fontSize:20, 
        color:'white',
    }
});

export default Heading;