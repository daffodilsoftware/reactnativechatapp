import { SEND_MESSAGE } from '../constants/actionConstants';


export function sendMessage(data){
	return{
		type: SEND_MESSAGE,
		payload: data
	}
}