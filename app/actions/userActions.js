import { GET_USER_NAME, } from '../constants/actionConstants';

export function getUserName(text){
    return{
        type: GET_USER_NAME,
        payload: text
    }
}