"use strict";

import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    TouchableHighlight,
    Dimensions,
    TextInput,
    Keyboard,
    Image
} from  'react-native';
import { connect } from 'react-redux';
import socket from '../../socket/socket';
import dismissKeyboard from 'react-native-dismiss-keyboard';

class SendMessageContainer extends Component{
    constructor(props){
        super(props);
        this.state = {
            messageToBeSent:''
        };
    }


    onPress=()=>{
        dismissKeyboard();
        if(this.state.messageToBeSent !==''){
            socket.emit('new-message', {message: this.state.messageToBeSent, sendBy: this.props.username, time: new Date()});
            this.setState({messageToBeSent:''});
        }
    }
    render(){
        return(
            <View style={styles.inputMainContainer}>
                <View style={styles.textContainer}>
                    <TextInput
                        style={styles.inputStyle}
                        value={this.state.messageToBeSent}
                        multiline={true}
                        underlineColorAndroid="rgba(0,0,0,0.0)"
                        onChangeText={(text) => this.setState({messageToBeSent:text})}
                    />
                </View>
                <View style={styles.sendContainer}>
                    <TouchableHighlight underlayColor={'#4e4273'} onPress={this.onPress}>
                        <Image style={styles.sendingLabel} source={require('../../assets/images/send.png')}  resizeMode='contain' />
                    </TouchableHighlight>
                </View>
            </View>
        );
    }
}


/**
 * Styling to this component.
 * */

const styles = StyleSheet.create({
    inputMainContainer: {
        flex: 1,
        flexDirection:'row',
        backgroundColor:'#90A4AE'
    },
    textContainer: {
        flex: 1,
        justifyContent: 'center',
        marginLeft:10,
        marginRight:5
    },
    sendContainer: {
        justifyContent: 'center',
        marginRight:5,

    },
    sendingLabel: {
        color: '#ffffff',
        height: 30,
        width: 30,
        fontSize: 15,
        justifyContent: 'center',
        alignItems:'center',
        backgroundColor:'#BC0000',
        paddingTop: 5,
        borderRadius:3,
        padding:5
    },
    inputStyle: {
        color: 'black',
        paddingRight: 10,
        paddingLeft: 10,
        paddingTop: 5,
        height: 32,
        backgroundColor:'#EEEEEE'
    }
})



/**
 * connect() connects a react component to redux store.
 * It takes two functions as arguments; the first argument to fetch values from store and the second argument to dispatch action methods. 
 * */

export default connect(state => ({
      username:state.user.name
    }))(SendMessageContainer);