'use strict';
import React, { Component } from 'react';
import {AppRegistry, StyleSheet, TouchableHighlight, Text, ListView, View, ScrollView} from 'react-native';
import { connect } from 'react-redux';
import TextMessageContainer from './textMessageContainer';

const ds = new ListView.DataSource({
    rowHasChanged: (r1, r2) => r1 !== r2
});

class Chat extends Component {

    constructor(props) {
        super(props);
        this.state = {
            dataSource: ds.cloneWithRows(this.props.messages),
            listHeight: 0,
            scrollViewHeight: 0
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            dataSource: this.state.dataSource.cloneWithRows(nextProps.messages)
        });
    }

    renderRow=(data)=> {
        return (<TextMessageContainer message={data} /> );
    }


     render() {
            return (

                    <ScrollView
                        keyboardDismissMode="on-drag"
                        onContentSizeChange={ (contentWidth, contentHeight) => {this.setState({listHeight: contentHeight })} }
                        onLayout={(e) => {this.setState({scrollViewHeight: e.nativeEvent.layout.height })} }
                        ref={ (ref) => this.scrollView = ref }>
                        <ListView
                            dataSource={this.state.dataSource}
                            renderRow={this.renderRow}
                            enableEmptySections={true}
                        />
                    </ScrollView>

             );
     }

    componentDidUpdate(){
        this.scrollToEndOfScreen();
    } 

    scrollToEndOfScreen(){
        this.scrollView.scrollTo({ y: this.state.listHeight - this.state.scrollViewHeight })
    }
}


/**
 * connect() connects a react component to redux store.
 * It takes two functions as arguments; the first argument to fetch values from store and the second argument to dispatch action methods. 
 * */

export default connect(state => ({
      messages: state.messages.messages
    }))(Chat);