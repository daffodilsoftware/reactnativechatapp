import React, { Component } from 'react';
import { View, Text, TextInput, Dimensions, StyleSheet,TouchableHighlight, Image, Alert, Modal, Keyboard, } from 'react-native';
import { Button } from 'native-base';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { getUserName } from '../../actions/userActions';
import Heading from '../../components/heading';
import dismissKeyboard from 'react-native-dismiss-keyboard';

var windowWidth = Dimensions.get('window').width;

class RetrieveUserName extends Component{
    constructor(props){
        super(props);
        this.state ={
            text:'',
            modalVisible: true
        }
    }

    /***
     * handler to send to chat view.
     */

    onPress=()=>{
        if(this.state.text.trim() !==''){
            this.props.getUserName(this.state.text);
            this.setState({
                text:'',
                modalVisible: false
            });
        }
        else
            Alert.alert("Please enter your name to proceed.");

    }

    render(){
        return(
            
            <Modal animationType={"slide"} transparent={false} visible={this.state.modalVisible}>
                <Heading />
                <View style={styles.container}>  
                    <Image style={styles.imgStyle} source={require('../../assets/images/welcome.png')}  resizeMode='contain' />
                    <View style={styles.innerContainer}>
                        <TextInput
                            style={styles.textInputStyle}
                            placeholder="Please enter your name"
                            underlineColorAndroid="rgba(0,0,0,0.0)"
                            onChangeText={(text) => this.setState({text})}
                            value={this.state.text}
                        />
                    </View>
                    <View style={styles.innerContainer}>
                        <Button rounded style={styles.buttonStyle} onPress={this.onPress}> Next </Button>
                    </View>
                </View>    
            </Modal>
            
        );
    }
}

/**
 * Styling to this component.
 * */

const styles = StyleSheet.create({
   container: {
        alignItems:'center',
        justifyContent:'center', 
        alignSelf:'center', 
        margin:50
    },
    imgStyle: {
        height:120, 
        width:120
    },
    innerContainer: {
        paddingTop:20
    },
    textInputStyle: {
        borderColor: '#BC0000',
        backgroundColor:'#EEEEEE', 
        borderWidth: 1,
        borderRadius:10,
        height:40,
        width:windowWidth-130,
        paddingLeft:10
    },
    buttonStyle: {
        width:windowWidth-130,
        backgroundColor:'#BC0000'
    }
})


/**
 * connect a react component to redux store.
 * It takes two functions as arguments; the first argument to fetch values from store and the second argument to dispatch action methods. 
 * */
export default connect(null, 
    (dispatch) => ({
      getUserName:bindActionCreators(getUserName, dispatch)
    })
)(RetrieveUserName);