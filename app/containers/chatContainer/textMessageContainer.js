'use strict';

import React, { Component } from 'react';
import moment from 'moment';
import { connect } from 'react-redux';

import socket from '../../socket/socket';
import {AppRegistry, StyleSheet, TouchableHighlight, Text, View, Dimensions, ScrollView } from 'react-native';

const { width } = Dimensions.get('window');

class TextMessageContainer extends Component {
    render() {
        return(
            <View style={styles.talkBubble}>
                    {this.props.username === this.props.message.sendBy  ?
                        <View style={{marginRight:20}}>
                            <View style={[styles.messageBlockRight, styles.talkBubbleSquare]}>
                                <Text style={[styles.textRight]}>
                                    {this.props.message.sendBy}
                                </Text>
                                <Text style={styles.text}>
                                    {this.props.message.message}
                                </Text>
                                <Text style={[styles.time ,styles.timeRight]}>{moment(this.props.message.time).format('hh:mm a')}</Text>

                            </View> 
                            <View style={styles.talkBubbleTriangleRight} />
                        </View>

                        :
                        <View style={{marginLeft:20}}>
                            <View style={[styles.talkBubbleSquare, styles.messageBlock]}>
                                <Text style={styles.textLeft}>
                                    {this.props.message.sendBy}
                                </Text>
                                <Text style={styles.textreciver}>
                                    {this.props.message.message}
                                </Text>

                                <Text style={[styles.time ,styles.timeLeft]}>{moment(this.props.message.time).format('hh:mm a')}</Text>
                            </View>
                            <View style={styles.talkBubbleTriangle} />
                        </View>
                    }

            </View>
        )
    }
}

const styles = StyleSheet.create({
    
    messageBlock: {
        flexDirection: "column",
        padding: 5,
        justifyContent: "center",
        alignSelf: "flex-start",
        borderRadius: 6,
        marginBottom: 5
    },
    messageBlockRight: {
        flexDirection: "column",
        backgroundColor: "white",
        padding: 5,
        justifyContent: "flex-end",
        alignSelf: "flex-end",
        borderRadius: 6,
        marginBottom: 5
    },
    text: {
        color: "#5c5c5c",
        alignSelf: "flex-start"
    },
    textreciver: {
        color: "#5c5c5c",
        alignSelf: "flex-start",
        textAlign: "right"
    },
    time: {
        alignSelf: "flex-end",
        marginTop:5,
        fontSize:12,

    },
    timeRight: {
        alignSelf: "flex-end",
        color: "#BC0000",

    },
    timeLeft:{
        alignSelf: "flex-end",
        color: "green",

    },
    textRight: {
        color: "#BC0000",
        alignSelf: "flex-start",
        textAlign: "right"
    },
    textLeft: {
        color: "green",
        alignSelf: "flex-start",
        textAlign: "left"
    },
    input:{
        borderTopColor:"#e5e5e5",
        borderTopWidth:1,
        padding:10,
        flexDirection:"row",
        justifyContent:"space-between"
    },
    textInput:{
        height:30,
        width:(width * 0.85),
        color:"#e8e8e8",
    },
    talkBubble: {
      backgroundColor: 'transparent'
    },
    talkBubbleSquare: {
      flex : 1,
      width: null,
      height: null,
      backgroundColor: 'white',
      borderRadius: 10,
      padding: 10,
    },
    talkBubbleTriangle: {
      position : 'absolute',
      left: -20,
      top: 20,
      width: 0,
      height: 0,
      borderTopColor: 'transparent',
      borderTopWidth: 10,
      borderRightWidth: 20,
      borderRightColor: 'white',
      borderBottomWidth: 10,
      borderBottomColor: 'transparent'
    },
    talkBubbleTriangleRight: {
      position : 'absolute',
      right: -20,
      top: 20,
      width: 0,
      height: 0,
      borderTopColor: 'transparent',
      borderTopWidth: 10,
      borderRightWidth: 20,
      borderRightColor: 'white',
      borderBottomWidth: 10,
      borderBottomColor: 'transparent',
      transform: [
      {rotate: '180deg'}
    ]
    }
});

/**
 * connect() connects a react component to redux store.
 * It takes two functions as arguments; the first argument to fetch values from store and the second argument to dispatch action methods. 
 * */
export default connect(state => ({
      username:state.user.name
    }))(TextMessageContainer);