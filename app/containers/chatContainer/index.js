import React, { Component } from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { connect } from 'react-redux';
import SendMessageContainer from './sendMessageContainer';
import Chat from './chat';
import Heading from '../../components/heading';
import RetrieveUserName from './retrieveUserName';

/* *
 * This is simple conatiner to combine all sub components in a chat component 
 * */

class Index extends Component{
   render(){
      return(
         <View style={styles.mainContainer} >
            <RetrieveUserName/> 
            <View style={styles.headingContainer}>
              <Heading />
            </View>
            <View style={styles.chatContianer}>
              <Chat/>
            </View>
             <View style={styles.sendMessageContainer}>
                 <SendMessageContainer socket={ this.socket } />
             </View>
         </View>
      );
  }
}

/**
 * Styling to this component.
 * */
const styles = StyleSheet.create({
    mainContainer:{
        flex:1,
        flexDirection:'column',
        backgroundColor:'#EEEEEE'
    },
    sendMessageContainer:{
        flex:1,
    },
    chatContianer:{
        flex:8,
        margin:10
    },
    headingContainer:{
        flex:1,
    }
});


/**
 * connect() connects a react component to redux store.
 * It takes two functions as arguments; the first argument to fetch values from store and the second argument to dispatch action methods. 
 * */

export default connect(state => ({
      user:state.user
    }))(Index);