import { combineReducers } from 'redux';
import messageList from './messageList';
import userName from './userName';

const rootReducer = combineReducers({
    messages: messageList,
    user: userName
});

export default rootReducer;