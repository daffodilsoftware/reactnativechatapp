import { GET_USER_NAME } from '../constants/actionConstants';

const initialState ={
    name:''
};

export default function userName(state=initialState , action){
    switch (action.type) {
        case GET_USER_NAME:
            return {...state, name: action.payload }
        
        default:
            return state ;
    }
}
