import { SEND_MESSAGE } from '../constants/actionConstants';

const initialState = {
    messages:[],

}

export default function messageList(state = initialState , action){
    switch (action.type) {
        case SEND_MESSAGE:
            return {...state, messages:state.messages.concat(action.payload)  };
      
        default:
            return state ;
    }
}
