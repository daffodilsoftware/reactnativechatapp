import React from 'react';
import  { AppRegistry } from 'react-native';
import { Provider } from 'react-redux';
import store from './app/configuration/store';
import Index from './app/components/index';


const App = () => (
    <Provider store={store}>
        <Index />
    </Provider>
)

 
AppRegistry.registerComponent('RNChatApp', () => App);
